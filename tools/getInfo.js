const SQL_HOST = "localhost"; //localhost
const SQL_USER = "root"; //thousand
const SQL_PASS = "jeff"; //1234
const SQL_NFC_DB = "nfc"; //smartcard_db
module.exports = {

    dbinfo: (dbsel) => {

        let DBSel = SQL_NFC_DB; 

        switch (dbsel) {
            case "nfc":
                DBSel = SQL_NFC_DB;
                break;
        }

        return {
            host: SQL_HOST,
            user: SQL_USER,
            password: SQL_PASS,
            database: DBSel
        };
    },
    SQLQuery: (con, Query, Insert) => {
        return new Promise((resolve, reject) => {
            con.query(Query, Insert, function (err, results, fields) {
                if (err) err;
                resolve(results);
            });
        });
    },
    SQLExecute: (con, Commend, data) => {
        return new Promise((resolve, reject) => {
            con.query(Commend, data, function (err, results) {
                if (err) throw err;
                resolve({ status: true, id: results.insertId });
            });
        });
    }
};