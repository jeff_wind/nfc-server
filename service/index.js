const mysql = require('mysql');
const sqlinfo = require('../tools/getInfo.js');
var sd = require('silly-datetime');
const DB_INFO_NFC = sqlinfo.dbinfo("nfc");
var ip = require("ip");
const fs = require('fs');
module.exports = {
    /*Login: async(input, callback) => {

        //回傳資料格式
        let output = (status, message, card_id, name) => {
            let responce = {
                status: status,
                message: message,
                card_id: card_id,
                name: name
            };
            try {
                if (typeof connection != 'undefined' && connection != null)
                    connection.end();

            } catch (e) {
                console.log(e);
                responce.status = 13;
                responce.message = "sql not connet";
            }
            return JSON.stringify(responce);
        };
        try {
            let connection = mysql.createConnection(DB_INFO_NFC);
            connection.connect();
            if (connection == null) {
                callback(null, output(10, "failed get database connection", ""));
                return;
            }
            let SQLStr = "";
            let SQLFlag = "";
            let SQLData = [];

            let card_id = input.card_id || "";
            console.log(card_id);
            // let name = input.card_id || "";
            //欄位輸入錯誤
            if (card_id == "") {
                callback(null, output(31, "field error"));
                return;
            }

            //檢查是否有帳號
            SQLStr = 'SELECT card_id, name FROM card_info WHERE card_id = ? ';
            SQLData = await Promise.resolve(sqlinfo.SQLQuery(connection, SQLStr, [card_id]));

            if (SQLData.length == 0) {
                callback(null, output(11, "SQLData is not found", ""));
                console.log("SQLData is not found");
                return;
            }

            card_id = SQLData[0].card_id;
            let name = SQLData[0].name;

            console.log(SQLData[0]);

            let memberData = [];
            memberData.push({
                card_id: card_id,
                name: name
            });

            //console.log(cardID);
            callback(null, output(0, "success", card_id, name));
        } catch (error) {
            console.log(error);
            callback(null, output(20, "service unknow failed!!"));
        }
    },
    Record: async(input, callback) => {

        //回傳資料格式
        let output = (status, message, data) => {
            let responce = {
                status: status,
                message: message,
                data: data
            };
            try {
                if (typeof connection != 'undefined' && connection != null)
                    connection.end();

            } catch (e) {
                console.log(e);
                responce.status = 13;
                responce.message = "sql not connet";
            }
            return JSON.stringify(responce);
        };
        try {
            let connection = mysql.createConnection(DB_INFO_NFC);
            connection.connect();
            if (connection == null) {
                callback(null, output(10, "failed get database connection", ""));
                return;
            }
            let SQLStr = "";
            let SQLFlag = "";
            let SQLData = [];

            let card_id = input.card_id || "";
            // let name = input.card_id || "";
            console.log(input);
            //欄位輸入錯誤
            if (card_id == "") {
                callback(null, output(31, "field error"));
                return;
            }

            SQLStr = 'INSERT INTO induction_log(card_id, member_id, time ) VALUES (?, ?, NOW()) ';
            SQLData = await Promise.resolve(sqlinfo.SQLQuery(connection, SQLStr, [card_id, "ttt"]));
            // let memberData = [];
            // memberData.push({
            //     card_id: card_id,
            //     name: name
            // });

            //console.log(cardID);
            callback(null, output(0, "success"));
        } catch (error) {
            console.log(error);
            callback(null, output(20, "service unknow failed!!"));
        }
    },*/
    GetUserData: async(input, callback) => {
        //回傳資料格式
        let output = (status, message, memberData) => {
            let responce = {
                status: status,
                message: message,
                memberData: memberData
            };
            try {
                if (typeof connection != 'undefined' && connection != null)
                    connection.end();

            } catch (e) {
                console.log(e);
                responce.status = 13;
                responce.message = "sql not connet";
            }
            return JSON.stringify(responce);
        };
        try {
            let connection = mysql.createConnection(DB_INFO_NFC);
            connection.connect();
            if (connection == null) {
                callback(null, output(10, "failed get database connection", ""));
                return;
            }
            let SQLStr = "";
            let SQLFlag = "";
            let SQLData = [];

            let card_id = input.card_id || "";
            console.log(card_id);
            //欄位輸入錯誤
            if (card_id == "") {
                callback(null, output(31, "field error"));
                return;
            }

            //檢查是否有帳號
            SQLStr = 'SELECT card_id, name, student_id FROM card_info WHERE card_id = ? ';
            SQLData = await Promise.resolve(sqlinfo.SQLQuery(connection, SQLStr, [card_id]));

            if (SQLData.length == 0) {
                callback(null, output(11, "SQLData is not found", ""));
                console.log("SQLData is not found");
                return;
            }

            card_id = SQLData[0].card_id;
            let name = SQLData[0].name;
            let student_id = SQLData[0].student_id

            console.log(SQLData[0]);

            let memberData = [];
            memberData.push({
                card_id: card_id,
                name: name,
                student_id: student_id
            });

            //console.log(cardID);
            callback(null, output(0, "success", memberData));
        } catch (error) {
            console.log(error);
            callback(null, output(20, "service unknow failed!!"));
        }
    },
    GetUserDataWithoutCard: async(input, callback) => {
        //回傳資料格式
        let output = (status, message, memberData) => {
            let responce = {
                status: status,
                message: message,
                memberData: memberData
            };
            try {
                if (typeof connection != 'undefined' && connection != null)
                    connection.end();

            } catch (e) {
                console.log(e);
                responce.status = 13;
                responce.message = "sql not connet";
            }
            return JSON.stringify(responce);
        };
        try {
            let connection = mysql.createConnection(DB_INFO_NFC);
            connection.connect();
            if (connection == null) {
                callback(null, output(10, "failed get database connection", ""));
                return;
            }
            let SQLStr = "";
            let SQLFlag = "";
            let SQLData = [];

            let card_id = input.card_id || "";
            let birth = input.birth || "";
            let type = input.type || "";

            //欄位輸入錯誤
            if (card_id == "") {
                callback(null, output(31, "field error"));
                return;
            }

            //檢查是否有帳號
            SQLStr = 'SELECT card_id, name, student_id FROM card_info WHERE identity_card like "%' + card_id + '" and birth = ?';
            SQLData = await Promise.resolve(sqlinfo.SQLQuery(connection, SQLStr, [birth]));

            if (SQLData.length == 0) {
                callback(null, output(11, "SQLData is not found", ""));
                console.log("SQLData is not found");
                return;
            }

            card_id = SQLData[0].card_id;
            let name = SQLData[0].name;
            let student_id = SQLData[0].student_id
            var time = sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss');
            let memberData = [];
            memberData.push({
                card_id: card_id,
                name: name,
                student_id: student_id,
                type: type,
                create_time: time
            });
            callback(null, output(0, "success", memberData));
        } catch (error) {
            console.log(error);
            callback(null, output(20, "service unknow failed!!"));
        }
    },
    UploadTempData: async(input, callback) => {
        //回傳資料格式
        let output = (status, message) => {
            let responce = {
                status: status,
                message: message
            };
            try {
                if (typeof connection != 'undefined' && connection != null)
                    connection.end();

            } catch (e) {
                console.log(e);
                responce.status = 13;
                responce.message = "sql not connet";
            }
            return JSON.stringify(responce);
        };
        try {
            let connection = mysql.createConnection(DB_INFO_NFC);
            connection.connect();
            if (connection == null) {
                callback(null, output(10, "failed get database connection", ""));
                return;
            }
            let SQLStr = "";
            let SQLFlag = "";
            let SQLData = [];

            let card_id = input.card_id || "";
            let temp = input.temp || "";
            console.log(input);

            //欄位輸入錯誤
            if (card_id == "" || temp == "") {
                callback(null, output(31, "field error"));
                return;
            }

            //檢查是否有帳號
            SQLStr = 'INSERT INTO temp_log(card_id, temp, create_time ) VALUES (?, ?, NOW())';
            SQLData = await Promise.resolve(sqlinfo.SQLQuery(connection, SQLStr, [card_id, temp]));

            if (SQLData.length == 0) {
                callback(null, output(11, "SQLData is not found", ""));
                console.log("SQLData is not found");
                return;
            }
            //console.log(cardID);
            callback(null, output(0, "success"));
        } catch (error) {
            console.log(error);
            callback(null, output(20, "service unknow failed!!"));
        }
    },
    //新增客人資料
    AddGuest: async(input, callback) => {

        //回傳資料格式
        let output = (status, message, data) => {
            let responce = {
                status: status,
                message: message,
                data: data
            };
            try {
                if (typeof connection != 'undefined' && connection != null)
                    connection.end();

            } catch (e) {
                console.log(e);
                responce.status = 13;
                responce.message = "sql not connet";
            }
            return JSON.stringify(responce);
        };
        try {
            let connection = mysql.createConnection(DB_INFO_NFC);
            connection.connect();
            if (connection == null) {
                callback(null, output(10, "failed get database connection", ""));
                return;
            }
            let SQLStr = "";
            let SQLFlag = "";
            let SQLData = [];

            let name = input.name || "";
            let phone = input.phone || "";
            let position = input.position || "";
            let note = input.note || "";
            let src = input.src || "";
            let dst = input.dst || "";
            let type = input.type || ""; //0 正常 1 永久

            //欄位輸入錯誤
            if (name == "" && phone == "") {
                callback(null, output(31, "field error"));
                return;
            }
            console.log(input);
            SQLStr = 'INSERT INTO guest_info(name, phone, position, note, src, dst, type, ip, create_time) VALUES (?, ?, ?, ?, ?, ?, ?, ?, NOW()) ';
            SQLData = await Promise.resolve(sqlinfo.SQLQuery(connection, SQLStr, [name, phone, position, note, src, dst, type, ip.address()]));
            var time = sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss');
            let memberData = [];
            memberData.push({
                name: name,
                phone: phone,
                type: type,
                create_time: time
            });

            callback(null, output(0, "success", memberData));
        } catch (error) {
            console.log(error);
            callback(null, output(20, "service unknow failed!!"));
        }
    },
}