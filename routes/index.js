var express = require('express');
var router = express.Router();
var index = require('../service/index');

//外賓註冊
router.get('/', function(req, res, next) {
    res.render('registered');
});
//長時間註冊
router.get('/LongTimereGistered', function(req, res, next) {
    res.render('LongTimereGistered');
});

router.post('/LOGIN', function(req, res, next) {
    // console.log("test");
    index.Login(req.body, (err, responce) => {
        res.send(responce);
    });
});


router.post('/GetUserData', function(req, res, next) {
    index.GetUserData(req.body, (err, responce) => {
        res.send(responce);
    });
});

router.post('/GetUserDataWithoutCard', function(req, res, next) {
    index.GetUserDataWithoutCard(req.body, (err, responce) => {
        res.send(responce);
    });
});

router.post('/UploadTempData', function(req, res, next) {
    index.UploadTempData(req.body, (err, responce) => {
        res.send(responce);
    });
});

router.post('/AddGuest', function(req, res, next) {
    index.AddGuest(req.body, (err, responce) => {
        res.send(responce);
    });
});

module.exports = router;